﻿Public Class PlayButton
    Private _sound As Sounds.Sound

    Public Event PrimaryClick(sender As Object, e As RoutedEventArgs)
    Event SecondaryClick(sender As Object, e As MouseButtonEventArgs)

    Public Event EditClick(sender As Object, e As RoutedEventArgs)
    Public Event RemoveClick(sender As Object, e As RoutedEventArgs)

    Public ReadOnly Property Sound As Sounds.Sound
        Get
            Return _sound
        End Get
    End Property

    Public Sub AssignSound(snd As Sounds.Sound)
        Me._sound = snd
        AddHandler snd.PlayStateChanged, AddressOf Sound_PlayStateChanged
        AddHandler snd.Updated, AddressOf UpdateUI
        UpdateUI()
    End Sub

    Delegate Sub UpdateUI_Delegate()
    Private Sub UpdateUI()
        If Me.Dispatcher.CheckAccess Then
            Me.UI_Name.Text = _sound.Name
            Me.UI_Name.ToolTip = _sound.Name

            If Sound.GotHotkeyAssigned Then
                Me.UI_Shortcut.Text = _sound.Hotkey.ToString
            Else
                Me.UI_Shortcut.Text = String.Empty
            End If
        Else
            Me.Dispatcher.Invoke(New UpdateUI_Delegate(AddressOf UpdateUI))
        End If
    End Sub

    Private Sub Sound_PlayStateChanged()
        If Sound.PlayState Then
            Me.BorderBrush = Brushes.DodgerBlue
        Else
            Me.BorderBrush = Brushes.White
        End If
    End Sub

    Private Sub PlayButton_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        SecondLayout = False
    End Sub

    Private Sub MouseButtonUp(sender As Object, e As MouseButtonEventArgs) Handles Me.MouseUp
        If e.ChangedButton = MouseButton.Left Then Return
        'Only Secondary Button
        ReleaseMouseCapture()
        'Only If Over = Clicked ?
        If (Me.IsMouseOver) Then RaiseEvent SecondaryClick(sender, e)
    End Sub

    Private Sub MouseButtonDown(sender As Object, e As MouseButtonEventArgs) Handles Me.MouseDown
        If e.ChangedButton = MouseButton.Left Then Return
        'Only Secondary Button
        CaptureMouse()
    End Sub

    Property SecondLayout As Boolean
        Get
            Return (EditLayout.Visibility = Windows.Visibility.Visible)
        End Get
        Set(value As Boolean)
            EditLayout.Visibility = If(value, Visibility.Visible, Visibility.Hidden)
            MainLayout.Visibility = If(Not value, Visibility.Visible, Visibility.Hidden)
        End Set
    End Property

    Private Sub SwitchLayouts() Handles Me.SecondaryClick
        SecondLayout = Not SecondLayout
    End Sub

    Private Sub Clicked(sender As Object, e As RoutedEventArgs) Handles Me.Click
        Dim button = CType(e.OriginalSource, Button)

        If button Is Me Then RaiseEvent PrimaryClick(sender, e)
    End Sub

    Private Sub DeleteButton_Click(sender As Object, e As RoutedEventArgs) Handles DeleteButton.Click
        SwitchLayouts()
        RaiseEvent RemoveClick(Me, e)
    End Sub

    Private Sub EditButton_Click(sender As Object, e As RoutedEventArgs) Handles EditButton.Click
        SwitchLayouts()
        RaiseEvent EditClick(Me, e)
    End Sub

    Private Sub PlayButton_MouseLeave(sender As Object, e As MouseEventArgs) Handles Me.MouseLeave
        SecondLayout = False
    End Sub
End Class
