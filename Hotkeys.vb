﻿Imports NHotkey
Imports NHotkey.Wpf

Public Class Hotkeys
    Public Event HotkeyInvoked(key As Hotkey)

    Private _List As New Dictionary(Of String, Hotkey)
    Public ReadOnly Property List As Dictionary(Of String, Hotkey)
        Get
            Return _List
        End Get
    End Property

    Public Function RegisterHotkey(shortcut As Hotkey) As Exception
        shortcut._Manager = Me
        'Generate name
        Dim Name As String = GenerateName()
        shortcut._Name = Name

        'Register hotkey
        Try
            HotkeyManager.Current.AddOrReplace(Name, shortcut.Key, shortcut.Modifiers, AddressOf HotkeyPressed)
            Trace.WriteLine(String.Format("Hotkey {0} registered successfully: {1} + {2}", Name, shortcut.Modifiers, shortcut.Key))

            If (_List.ContainsKey(Name)) Then
                'Cant happen
                _List(Name) = shortcut
            Else
                _List.Add(Name, shortcut)
            End If

            Return Nothing
        Catch regEx As HotkeyAlreadyRegisteredException
            shortcut._Manager = Nothing

            Trace.WriteLine(String.Format("Hotkey {0} cannot be registered - already used: {1} + {2}", Name, shortcut.Modifiers, shortcut.Key))
            Return New Exception("ALREADY_REGISTERED")
        Catch ex As Exception
            Trace.WriteLine("Unhandled exception: " + ex.Message)
            Return ex
        End Try
    End Function

    Public Function UnRegisterHotkey(shortcut As Hotkey) As Exception
        Try
            HotkeyManager.Current.Remove(shortcut.Name)
            Trace.WriteLine("Hotkey UnRegistered: " + shortcut.Name)

            'Cleaning
            List.Remove(shortcut.Key)
            shortcut.Dispose()
            shortcut = Nothing

            Return Nothing
        Catch ex As Exception
            Trace.WriteLine("Hotkey UnRegistration Failed: " + ex.Message)
            Return ex
        End Try
    End Function

    ''' <summary>
    ''' Function for generating random names for hotkey
    ''' </summary>
    ''' <param name="length"></param>
    ''' <returns>Generated name</returns>
    Private Function GenerateName(Optional length As Integer = 8) As String
        Dim isUnique = False
        Dim result As String = String.Empty

        While (result = String.Empty OrElse _List.ContainsKey(result))
            'The list of characters
            Dim chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            'Randomizer instance
            Dim random = New Random()
            'The generated string
            result = New String(Enumerable.Repeat(chars, length).[Select](Function(s) s(random.[Next](s.Length))).ToArray())
        End While

        Return result
    End Function

    ''' <summary>
    ''' ... When any of the hotkeys is pressed 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub HotkeyPressed(sender As Object, e As HotkeyEventArgs)
        Dim HotkeyName = e.Name

        If (_List.ContainsKey(HotkeyName)) Then
            Dim theHotkey = _List(HotkeyName)

            theHotkey.Trigger()
            RaiseEvent HotkeyInvoked(theHotkey) 'Not needed in higher ver

            e.Handled = True
        End If
    End Sub

    Class Hotkey : Implements IDisposable

        Public Event Triggered(hotkey As Hotkey)

        Friend _Manager As Hotkeys
        ''' <summary>
        ''' Who is the manager -> Assigned on registration
        ''' </summary>
        ''' <returns>The assigned manager</returns>
        Public ReadOnly Property Manager As Hotkeys
            Get
                Return _Manager
            End Get
        End Property

        Public ReadOnly Property Registered As Boolean
            Get
                Return Not IsNothing(_Manager)
            End Get
        End Property

        Friend _Name As String
        ''' <summary>
        ''' Name of the hotkey -> Assigned on registration (generated - random)
        ''' </summary>
        ''' <returns>The assigned name</returns>
        Public ReadOnly Property Name As String
            Get
                Return _Name
            End Get
        End Property

        Private _Key As Key
        Public ReadOnly Property Key As Key
            Get
                Return _Key
            End Get
        End Property

        Private _Modifiers As ModifierKeys
        Public ReadOnly Property Modifiers As ModifierKeys
            Get
                Return _Modifiers
            End Get
        End Property

        Friend Sub Trigger()
            Trace.WriteLine(String.Format("Hotkey '{0}' Pressed", _Name))
            RaiseEvent Triggered(Me)
        End Sub

        Sub New(key As Key, modifier As ModifierKeys)
            Me._Key = key : Me._Modifiers = modifier
        End Sub

        Public Function UnRegister() As Exception
            If Not IsNothing(_Manager) Then
                Return _Manager.UnRegisterHotkey(Me)
            Else
                Return New NullReferenceException
            End If
        End Function

        Public Overrides Function ToString() As String
            Return String.Format("{0} + {1}", Me._Modifiers.ToString, Me._Key.ToString)
        End Function

#Region "IDisposable Support"
        Private disposedValue As Boolean ' To detect redundant calls

        ' IDisposable
        Protected Overridable Sub Dispose(disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    _Name = Nothing
                    _Key = Nothing
                    _Modifiers = Nothing
                End If
            End If
            Me.disposedValue = True
        End Sub

        Public Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub
#End Region

    End Class
End Class
