﻿Imports System.Xml
Imports System.Text.RegularExpressions

Public Class Config
    Private theHotkeyRegistrant As Hotkeys
    Private theSoundRegistrant As Sounds

    Public ReadOnly Property HotkeyRegistrant As Hotkeys
        Get
            Return theHotkeyRegistrant
        End Get
    End Property
    Public ReadOnly Property SoundRegistrant As Sounds
        Get
            Return theSoundRegistrant
        End Get
    End Property

    Public Event ThemeChanged()

    Public Event LoadFailed(ex As Exception)
    Public Event LoadSuccessful()

    Private _cfgFile As String
    Public ReadOnly Property cfgFile As String
        Get
            Return _cfgFile
        End Get
    End Property

    Sub New(hk As Hotkeys, sn As Sounds)
        Me.theHotkeyRegistrant = hk : Me.theSoundRegistrant = sn
    End Sub

    '0 - 100
    Public Property MasterVolume As Integer = 100
    '0.0 - 1.0
    Public ReadOnly Property MasterVolume_Single As Single
        Get
            Return Convert.ToSingle(MasterVolume / 100)
        End Get
    End Property
    '0 - MAXDEVICEINDEX
    Public Property MasterDevice As Integer = 0

    Sub Save(file As String)
        Trace.WriteLine("Saving configuration...")
        Dim doc As New XmlDocument
        doc.AppendChild(doc.CreateXmlDeclaration("1.0", Nothing, Nothing))
        Dim root As XmlElement = DirectCast(doc.AppendChild(doc.CreateElement("soundboard")), XmlElement)
        root.SetAttribute("volume", MasterVolume.ToString)
        root.SetAttribute("device", MasterDevice.ToString)

        For Each snd In theSoundRegistrant.Playlist
            Dim soundElem As XmlElement = DirectCast(root.AppendChild(doc.CreateElement("sound")), XmlElement)
            'Set the name, if possible
            If Not snd.Name.Trim = String.Empty Then soundElem.SetAttribute("name", snd.Name.Trim)
            'Set the file - IMPORTANT
            soundElem.SetAttribute("file", snd.File)
            'Set hotkey, if possible
            If Not IsNothing(snd.Hotkey) Then
                soundElem.SetAttribute("key", snd.Hotkey.Key.ToString)

                Dim modifs As String = snd.Hotkey.Modifiers.ToString
                If modifs.Contains(ModifierKeys.Shift.ToString) Then soundElem.SetAttribute("shift", "true")
                If modifs.Contains(ModifierKeys.Alt.ToString) Then soundElem.SetAttribute("alt", "true")
                If modifs.Contains(ModifierKeys.Control.ToString) Then soundElem.SetAttribute("control", "true")
            End If
        Next

        'Save as UTF-8
        Using sw As IO.TextWriter = New IO.StreamWriter(file, False, Text.Encoding.UTF8)
            doc.Save(sw)
        End Using
    End Sub

    Sub LoadAsync(cfgFile As String)
        Dim th As New System.Threading.Thread(AddressOf Load)
        th.Start(cfgFile)
    End Sub

    Private Sub Load(cfgFile As String)
        Me._cfgFile = cfgFile

        If Not IO.File.Exists(cfgFile) Then
            RaiseEvent LoadFailed(New IO.FileNotFoundException)
            Return
        End If

        Dim doc As New XmlDocument

        Try
            doc.Load(cfgFile)
        Catch ex As Exception
            'Format issue
            RaiseEvent LoadFailed(ex)
            Return
        End Try

        Try
            Dim root = doc.DocumentElement

            If Not root.Name = "soundboard" Then Throw New Exception

            'Get IMPORTANT vals from xml
            Dim attr_volume = root.GetAttribute("volume")
            Dim attr_device = root.GetAttribute("device")

            'Load needed vals - IMPORTANT
            If attr_volume.Trim = String.Empty Then Throw New Exception
            If attr_device.Trim = String.Empty Then Throw New Exception
            If Not Integer.TryParse(attr_volume, MasterVolume) Then Throw New Exception
            If Not Integer.TryParse(attr_device, MasterDevice) Then Throw New Exception

            'Use defaults, if invalid
            If MasterVolume > 100 OrElse MasterVolume < 0 Then MasterVolume = 100 'I'll make u BlowExPlode
            If MasterDevice < 0 OrElse MasterDevice >= NAudio.Wave.WaveOut.DeviceCount Then MasterDevice = 0 '0 = default ?

            Dim sounds = root.SelectNodes("//sound")

            For Each sound As XmlElement In sounds
                'Can't contiunue without file specified so...
                Dim file As String = sound.GetAttribute("file").Trim
                If file = String.Empty Then Continue For

                'Create holder
                Dim snd As New Sounds.Sound(file)

                'Get XML info
                Dim attr_name As String = sound.GetAttribute("name").Trim : snd.AssignName(attr_name)
                Dim attr_key As String = sound.GetAttribute("key").Trim
                Dim attr_needShift = sound.GetAttribute("shift").Trim
                Dim attr_needAlt = sound.GetAttribute("alt").Trim
                Dim attr_needCtrl = sound.GetAttribute("control").Trim

                'Check for individual modifiers - are they "needed" ?
                Dim shift As Boolean = (attr_needShift.ToLower = "true")
                Dim alt As Boolean = (attr_needAlt.ToLower = "true")
                Dim ctrl As Boolean = (attr_needCtrl.ToLower = "true")

                'Find appropriate Key
                Dim triggerKey As Key
                Dim triggerKey_Success = [Enum].TryParse(attr_key, triggerKey)

                'Could find the key and one or more modifiers needed (Can't use just one key as a hotkey...)
                If triggerKey_Success And (shift OrElse alt OrElse ctrl) Then
                    'Valid keystoke
                    Dim buildModif As ModifierKeys = Nothing
                    If shift Then buildModif = buildModif Or ModifierKeys.Shift
                    If alt Then buildModif = buildModif Or ModifierKeys.Alt
                    If ctrl Then buildModif = buildModif Or ModifierKeys.Control

                    'Create hotkey holder
                    Dim stroke As New Hotkeys.Hotkey(triggerKey, buildModif)
                    'Register as hotkey
                    '->LazyInitializer Exception -> Fixed by running on UI thread :D
                    Application.Current.Dispatcher.Invoke(Sub()
                                                              theHotkeyRegistrant.RegisterHotkey(stroke)
                                                          End Sub)
                    'Assign to the sound
                    snd.AssignHotkey(stroke)

                    Trace.WriteLine("Added sound with assigned keystroke")
                Else
                    'No keystroke or invalid
                    Trace.WriteLine("Added sound without keystroke")
                End If

                'Add to the sound collection
                theSoundRegistrant.RegisterSound(snd, False)
            Next

            RaiseEvent LoadSuccessful()
        Catch ex As Exception
            RaiseEvent LoadFailed(New IO.FileFormatException)
            Return
        End Try
    End Sub
End Class
