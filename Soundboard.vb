﻿Imports NAudio
Imports NAudio.CoreAudioApi 'New API (Vista +)
Imports NAudio.Wave 'Old API

Public Class Soundboard
    ''' <summary>
    ''' Structure of Playback Devices, merging Old and New API
    ''' </summary>
    Class PlaybackDevice
        Private _DeviceNew As MMDevice
        Private _DeviceOld As Wave.WaveOutCapabilities
        Private _IsDefault As Boolean = False
        Private _ID As Integer

        Public ReadOnly Property IsDefault As Boolean
            Get
                Return _IsDefault
            End Get
        End Property

        Public ReadOnly Property ID As Integer
            Get
                Return _ID
            End Get
        End Property

        Public ReadOnly Property Device_New As MMDevice
            Get
                Return _DeviceNew
            End Get
        End Property

        Public ReadOnly Property Device_Old As Wave.WaveOutCapabilities
            Get
                Return _DeviceOld
            End Get
        End Property

        Public ReadOnly Property Faulty As Boolean
            Get
                Return (Device_New Is Nothing)
            End Get
        End Property

        Sub New(devId As Integer)
            If (devId > -1 And devId < Wave.WaveOut.DeviceCount) Then
                'Save for later
                _ID = devId

                'Get info about the device - from Old API
                _DeviceOld = Wave.WaveOut.GetCapabilities(devId)

                'Find connection between Old and New API - by device name
                Dim devicesWithName = (From device As MMDevice In (New MMDeviceEnumerator).EnumerateAudioEndPoints(DataFlow.Render, DeviceState.Active)
                        Where device.FriendlyName.StartsWith(_DeviceOld.ProductName)
                        Select device)

                'Connection found ?
                If devicesWithName.Count = 1 Then
                    'Save the connection
                    _DeviceNew = devicesWithName.FirstOrDefault

                    'Is Default Playback Device
                    _IsDefault = (New MMDeviceEnumerator).GetDefaultAudioEndpoint(DataFlow.Render, Role.Console).FriendlyName = _DeviceNew.FriendlyName
                Else
                    'Make invalid
                    _DeviceNew = Nothing
                End If
            Else
                'Dat timing bro
                Trace.WriteLine("Invalid device number - FU!")
            End If
        End Sub
    End Class

    Private WithEvents Player As Wave.WaveOut
    Private Reader As Wave.AudioFileReader

    ''' <summary>
    ''' Event that is triggered on completed sound play
    ''' </summary>
    ''' <param name="sound">Which sound</param>
    ''' <remarks></remarks>
    Public Event PlayEnded(sound As Sounds.Sound)

    Private _PlayedSound As Sounds.Sound = Nothing
    Private _PlaybackDevices As List(Of PlaybackDevice)
    Private _DefaultPlaybackDevice As PlaybackDevice = Nothing

    ''' <summary>
    ''' What sound is being played
    ''' </summary>
    ''' <value></value>
    ''' <returns>Actual sound structure</returns>
    Public ReadOnly Property PlayedSound As Sounds.Sound
        Get
            Return _PlayedSound
        End Get
    End Property

    ''' <summary>
    ''' Number of playback devices, through Old API
    ''' </summary>
    ''' <value></value>
    ''' <returns>Number of playback devices</returns>
    Private ReadOnly Property PlaybackDevices_Count As Integer
        Get
            Return WaveOut.DeviceCount
        End Get
    End Property

    ''' <summary>
    ''' Get list of playback devices through Old + New API
    ''' </summary>
    ''' <value></value>
    ''' <returns>Real playback devices</returns>
    ReadOnly Property PlaybackDevices() As List(Of PlaybackDevice)
        Get
            'Load the devices, if needed
            ReloadDevices()

            'Return the list
            Return _PlaybackDevices
        End Get
    End Property

    ''' <summary>
    ''' Volume of output from NAudio - possible to change ON-THE-FLY
    ''' </summary>
    ''' <value></value>
    ''' <returns>Actual volume</returns>
    Public Property Volume As Single
        Get
            If (Player IsNot Nothing) Then Return Player.Volume
            Return -1
        End Get
        Set(value As Single)
            If (Player IsNot Nothing) Then Player.Volume = value
        End Set
    End Property

    ''' <summary>
    ''' Device, that NAudio is rendering to - CAN'T CHANGE ON-THE-FLY
    ''' </summary>
    ''' <value></value>
    ''' <returns>Actual id of playback device</returns>
    Public Property Device As Integer
        Get
            If (Player IsNot Nothing) Then Return Player.DeviceNumber
            Return -1
        End Get
        Set(value As Integer)
            If (Player IsNot Nothing) Then Player.DeviceNumber = value
        End Set
    End Property

    ''' <summary>
    ''' Gets default playback device
    ''' </summary>
    ''' <returns>Default playback device, if possible, null otherwise</returns>
    ''' <remarks>Returns null if default playback device can't be find in New API</remarks>
    Function GetDefaultDevice() As PlaybackDevice
        Return _DefaultPlaybackDevice
    End Function

    ''' <summary>
    ''' Reloads list of playback devices, if list counts ain't equal
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ReloadDevices()
        'Does the collection exist ?
        If IsNothing(_PlaybackDevices) Then
            'Create it
            _PlaybackDevices = New List(Of PlaybackDevice)
        Else
            'Check if there's a need for re-load
            If (_PlaybackDevices.Count = PlaybackDevices_Count) Then Return
        End If

        'Reloading...
        'Clear old content
        _PlaybackDevices.Clear()
        _DefaultPlaybackDevice = Nothing

        'Iterate through info from Old API
        For deviceId As Integer = 0 To WaveOut.DeviceCount - 1
            'Create holder - connection between New & Old
            Dim pb = New Soundboard.PlaybackDevice(deviceId)
            'Add to collection if not faulty (= can't find connection - Old <-> New API)
            If Not pb.Faulty Then _PlaybackDevices.Add(pb)
            If Not pb.Faulty And pb.IsDefault Then _DefaultPlaybackDevice = pb
        Next
    End Sub

    ''' <summary>
    ''' Stops playing, destroys the player
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub KillPlayer()
        If (Player IsNot Nothing) Then
            If (Player.PlaybackState = Wave.PlaybackState.Playing) Then Player.Stop()
            Player.Dispose()
            Player = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Destroys the reader
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub KillReader()
        If (Reader IsNot Nothing) Then
            Reader.Dispose()
            Reader = Nothing
        End If
    End Sub

    ''' <summary>
    ''' Does clearing of all vars, player, reader etc.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub Clear()
        KillReader()
        KillPlayer()
        If Not IsNothing(_PlayedSound) Then _PlayedSound.SetPlayState(False)
        _PlayedSound = Nothing
    End Sub

    ''' <summary>
    ''' Plays specific sound, with select volume on selected device
    ''' </summary>
    ''' <param name="sound">The sound structure</param>
    ''' <param name="device">ID of desired device</param>
    ''' <param name="volume">Desired sound volume 0.0 - 1.0</param>
    ''' <remarks></remarks>
    Public Sub PlaySound(sound As Sounds.Sound, device As Integer, volume As Single)
        If IO.File.Exists(sound.File) Then
            Clear()

            Try
                Player = New Wave.WaveOut
                Reader = New Wave.AudioFileReader(sound.File)

                Player.DeviceNumber = device
                Player.Volume = volume
            Catch ex As Exception
                'FileUnsupported
                MessageBox.Show("File is not supported!", "Error")
                Return
            End Try

            Try
                Player.Init(Reader)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Error")
                Return
            End Try

            Try
                Player.Play()
                _PlayedSound = sound
                _PlayedSound.SetPlayState(True)
            Catch ex As Exception
                'File failed ?
                MessageBox.Show("This file cannot be played!", "Error")
                Return
            End Try
        End If
    End Sub

    ''' <summary>
    ''' Calls private cleaner
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub StopSound()
        ''Obsolete
        'If (Player IsNot Nothing) Then Player.Stop()

        Clear()
    End Sub

    Private Sub Player_PlaybackStopped(sender As Object, e As Wave.StoppedEventArgs) Handles Player.PlaybackStopped
        'Dokončeno přehrávání souboru
        Clear()
        RaiseEvent PlayEnded(_PlayedSound)
    End Sub
End Class
