﻿Public Class Keys
    Public Class ModifierKey
        Private _alt As Boolean = False
        Public ReadOnly Property Alt As Boolean
            Get
                Return _alt
            End Get
        End Property

        Private _control As Boolean = False
        Public ReadOnly Property Control As Boolean
            Get
                Return _control
            End Get
        End Property

        Private _shift As Boolean = False
        Public ReadOnly Property Shift As Boolean
            Get
                Return _shift
            End Get
        End Property

        Private _none As Boolean = False
        Public ReadOnly Property None As Boolean
            Get
                Return _none
            End Get
        End Property

        Sub New(modif As ModifierKeys)
            Dim modifs As String = modif.ToString

            _alt = modifs.Contains(ModifierKeys.Alt.ToString)
            _control = modifs.Contains(ModifierKeys.Control.ToString)
            _shift = modifs.Contains(ModifierKeys.Shift.ToString)
            _none = modifs.Contains(ModifierKeys.None.ToString)
        End Sub

        Sub New(alt As Boolean, control As Boolean, shift As Boolean)
            _alt = alt : _control = control : _shift = shift
            _none = Not (alt OrElse control OrElse shift)
        End Sub

        Public Overrides Function ToString() As String
            Return ToModifierKeys.ToString
        End Function

        Public Function ToModifierKeys() As ModifierKeys
            Dim ret As ModifierKeys = ModifierKeys.None

            If _alt Then ret = ret Or ModifierKeys.Alt
            If _control Then ret = ret Or ModifierKeys.Control
            If _shift Then ret = ret Or ModifierKeys.Shift

            Return ret
        End Function
    End Class

End Class
