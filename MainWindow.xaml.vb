﻿Imports System.Threading

Class MainWindow

    Private sb As Soundboard
    WithEvents cfg As Config

    Private hk As Hotkeys
    Private sn As Sounds

    Dim listenForUIchanges As Boolean = True

#Region "Window Start/End"
    Private Sub MainWindow_Closing(sender As Object, e As ComponentModel.CancelEventArgs) Handles Me.Closing
        If loading Then e.Cancel = True : Return 'Deny the close, don't continue
        cfg.Save("config.xml")
    End Sub

    Dim loading As Boolean = True
    Private Sub MainWindow_Loaded(sender As Object, e As RoutedEventArgs) Handles Me.Loaded
        ActiveStyle()
        loading = True
        SetActivePage(Pages.Loading)
        Dropper.Visibility = Windows.Visibility.Collapsed

        sb = New Soundboard : hk = New Hotkeys : sn = New Sounds
        AddHandler sn.TriggeredByHotkey, AddressOf SoundTriggered
        AddHandler sn.PlaylistUpdated, AddressOf PlaylistUpdated
        cfg = New Config(hk, sn)
        cfg.LoadAsync("config.xml")

        'Load playback devices
        Dim th As New Thread(AddressOf LoadDevices)
        th.Start()
    End Sub
#End Region

#Region "Playback Devices"
    Sub LoadDevices()
        'Do stuff, that takes too long
        Dim devs = sb.PlaybackDevices

        '... on other thread
        Me.listenForUIchanges = False
        For Each device In devs
            AddDevice(device)
        Next
        Me.listenForUIchanges = True
    End Sub

    Delegate Sub AddDevice_Delegate(device As Soundboard.PlaybackDevice)
    Sub AddDevice(device As Soundboard.PlaybackDevice)
        'Add on UI thread
        If Me.Dispatcher.CheckAccess Then
            Dim proInt As Integer = PlaybackDeviceListBox.Items.Add(New ComboBoxItem With {.Content = String.Format("{0} ({1})", device.Device_New.FriendlyName, device.ID), .Tag = device.ID})

            If (device.IsDefault) Then
                Trace.WriteLine("Default Device: " + device.Device_New.FriendlyName)
                PlaybackDeviceListBox.SelectedIndex = proInt
            End If

            If (device.ID = cfg.MasterDevice) Then PlaybackDeviceListBox.SelectedIndex = proInt
        Else
            Me.Dispatcher.Invoke(New AddDevice_Delegate(AddressOf AddDevice), device)
        End If
    End Sub
#End Region



#Region "Response from Config"
    Private Delegate Sub cfg_LoadFailed_Delegate(ex As Exception)
    Private Sub cfg_LoadFailed(ex As Exception) Handles cfg.LoadFailed
        If Me.Dispatcher.CheckAccess Then
            listenForUIchanges = False

            VolumeSlider.Value = cfg.MasterVolume

            listenForUIchanges = True

            If sn.Playlist.Count > 0 Then
                LoadSongs()
            Else
                ClearLoaded()
                LoadComplete()
            End If
        Else
            Me.Dispatcher.Invoke(New cfg_LoadFailed_Delegate(AddressOf cfg_LoadFailed), ex)
        End If
    End Sub

    Private Delegate Sub cfg_LoadSuccessful_Delegate()
    Private Sub cfg_LoadSuccessful() Handles cfg.LoadSuccessful
        If Me.Dispatcher.CheckAccess Then
            listenForUIchanges = False

            VolumeSlider.Value = cfg.MasterVolume

            listenForUIchanges = True

            If sn.Playlist.Count > 0 Then
                LoadSongs()
            Else
                ClearLoaded()
                LoadComplete()
            End If
        Else
            Me.Dispatcher.Invoke(New cfg_LoadSuccessful_Delegate(AddressOf cfg_LoadSuccessful))
        End If
    End Sub
#End Region

#Region "Base UI Control & Styling"
    Private Sub ActiveStyle() Handles Me.Activated, Me.Deactivated
        'Trace.WriteLine(If(Me.IsActive, "WindowActivated", "WindowDeactivated"))

        Me.BorderBrush = If(Me.IsActive, Brushes.DodgerBlue, Brushes.Gray)
        WindowTitler.Foreground = If(Me.IsActive, (New BrushConverter).ConvertFrom("#ff282828"), (New BrushConverter).ConvertFrom("#ff8893A0"))
    End Sub

    ''' <summary>
    ''' Changes the WindowChrome of selected Window (wnd) &amp; adjust its properties
    ''' </summary>
    ''' <param name="wnd">Window to affect</param>
    ''' <param name="glassThickness">Thickness of glass border (0 - no glass = no shadow, lower than 0 - whole window, higher than 0 - real border)</param>
    ''' <param name="resBorder">Thickness of resize border - where Windows natively supports resizing of the window</param>
    ''' <param name="topMove">Height of the window header/title - Windows native support for changing position of the window (from top), disables mouse events of controls underneath it</param>
    ''' <remarks></remarks>
    Public Sub NiceChrome(wnd As Window, glassThickness As Thickness, resBorder As Thickness, topMove As Integer)
        Me.WindowStyle = Windows.WindowStyle.SingleBorderWindow
        Shell.WindowChrome.SetWindowChrome(wnd, New Shell.WindowChrome With {.ResizeBorderThickness = resBorder, .GlassFrameThickness = glassThickness, .UseAeroCaptionButtons = False, .CaptionHeight = topMove, .CornerRadius = New CornerRadius(0), .NonClientFrameEdges = Shell.NonClientFrameEdges.None})
    End Sub

    Private Sub MainWindow_SourceInitialized(sender As Object, e As EventArgs) Handles Me.SourceInitialized, Me.SourceUpdated
        NiceChrome(Me, New Thickness(-1), New Thickness(1), 0)
    End Sub

    Private Sub Button_Close_Click(sender As Object, e As RoutedEventArgs) Handles Button_Close.Click
        Me.Close()
    End Sub

    Private Sub Button_Minimize_Click(sender As Object, e As RoutedEventArgs) Handles Button_Minimize.Click
        Me.WindowState = Windows.WindowState.Minimized
    End Sub

    Private Sub Window_TitleBar_MouseLeftButtonDown(sender As Object, e As MouseButtonEventArgs) Handles Window_TitleBar.MouseLeftButtonDown
        Me.DragMove()
    End Sub
#End Region

#Region "Loading/Playing Sounds"
    Private Sub PlaylistUpdated()
        LoadSongs()
    End Sub

    Delegate Sub ClearLoaded_Delegate()
    Private Sub ClearLoaded()
        If Me.Dispatcher.CheckAccess Then
            UI_List.Children.Clear()
        Else
            Me.Dispatcher.Invoke(New ClearLoaded_Delegate(AddressOf ClearLoaded))
        End If
    End Sub

    Delegate Sub LoadSong_Delegate(snd As Sounds.Sound)
    Private Sub LoadSong(snd As Sounds.Sound)
        If Me.Dispatcher.CheckAccess Then
            Dim elem As New PlayButton
            elem.AssignSound(snd)
            AddHandler elem.PrimaryClick, AddressOf PlayButton_Click
            AddHandler elem.EditClick, AddressOf PlayButton_Edit
            AddHandler elem.RemoveClick, AddressOf PlayButton_Remove
            UI_List.Children.Add(elem)
        Else
            Me.Dispatcher.Invoke(New LoadSong_Delegate(AddressOf LoadSong), snd)
        End If
    End Sub

    Private Sub LoadSongs_NewThread()
        ClearLoaded()
        For Each sound In sn.Playlist
            LoadSong(sound)
        Next
        LoadComplete()
    End Sub

    Private Sub LoadSongs()
        'UI_List.Children.Clear()
        'For Each sound In sn.Playlist
        '    Dim elem As New PlayButton
        '    elem.AssignSound(sound)
        '    AddHandler elem.PrimaryClick, AddressOf PlayButton_Click
        '    AddHandler elem.EditClick, AddressOf PlayButton_Edit
        '    AddHandler elem.RemoveClick, AddressOf PlayButton_Remove
        '    UI_List.Children.Add(elem)
        'Next

        Dim th As New System.Threading.Thread(AddressOf LoadSongs_NewThread)
        th.Start()
    End Sub

    Delegate Sub LoadComplete_Delegate()
    Sub LoadComplete()
        If Me.Dispatcher.CheckAccess Then
            loading = False
            SetActivePage(Pages.Main)
        Else
            Me.Dispatcher.Invoke(New LoadComplete_Delegate(AddressOf LoadComplete))
        End If
    End Sub

    Private Sub PlayButton_Click(sender As Object, e As RoutedEventArgs)
        Dim btn As PlayButton = CType(sender, PlayButton)

        SoundTriggered(btn.Sound)
        'Trace.WriteLine("Wanna play sound: " + btn.Sound.Name)
    End Sub

    Private Sub PlayButton_Edit(sender As Object, e As RoutedEventArgs)
        Dim btn = DirectCast(sender, PlayButton)
        Dim snd = btn.Sound
        Trace.WriteLine("Edit " + snd.Name)

        TriggerEdit(snd)
    End Sub

    Private Sub PlayButton_Remove(sender As Object, e As RoutedEventArgs)
        Dim btn = DirectCast(sender, PlayButton)
        Dim snd = btn.Sound

        If snd.PlayState Then sb.StopSound()
        sn.UnRegisterSound(snd)
    End Sub

    Private Sub SoundTriggered(snd As Sounds.Sound)
        If Not snd.File = String.Empty Then
            If Not IsNothing(sb.PlayedSound) Then
                If snd Is sb.PlayedSound Then
                    sb.StopSound()
                Else
                    sb.PlaySound(snd, cfg.MasterDevice, cfg.MasterVolume_Single)
                End If
            Else
                sb.PlaySound(snd, cfg.MasterDevice, cfg.MasterVolume_Single)
            End If
        Else
            sb.StopSound()
        End If
    End Sub
#End Region

#Region "Tabs"
    Enum Pages
        Settings
        Main
        EditSound
        Loading
    End Enum
    Sub SetActivePage(page As Pages)
        Dim _pages As New List(Of Grid) From {Page_Main, Page_Settings, Page_EditSound, Page_Loading}
        Dim _tabs As New List(Of Button) From {Tabber_Home, Tabber_Settings}
        Dim _holders As New List(Of Border) From {TabberHolder_Home, TabberHolder_Settings}

        'Hide all pages
        For Each pg In _pages
            pg.Visibility = Windows.Visibility.Collapsed
        Next
        'Clear colors of tabs
        For Each hd In _holders
            hd.BorderBrush = Brushes.Transparent
        Next

        Dim _tabToDisable As Button = Nothing

        Select Case page
            Case Pages.Main
                Page_Main.Visibility = Windows.Visibility.Visible
                Page_Main_NoSounds.Visibility = If(IsNothing(sn) OrElse sn.Playlist.Count = 0, Visibility.Visible, Visibility.Collapsed)
                TabberHolder_Home.BorderBrush = Brushes.DodgerBlue
                _tabToDisable = Tabber_Home
            Case Pages.Settings
                Page_Settings.Visibility = Windows.Visibility.Visible
                TabberHolder_Settings.BorderBrush = Brushes.DodgerBlue
                _tabToDisable = Tabber_Settings
            Case Pages.EditSound
                Page_EditSound.Visibility = Windows.Visibility.Visible
            Case Pages.Loading
                Page_Loading.Visibility = Windows.Visibility.Visible
        End Select

        If page = Pages.EditSound Then
            [AddHandler](Keyboard.KeyDownEvent, DirectCast(AddressOf HandleKeyDownEvent, KeyEventHandler))
        Else
            [RemoveHandler](Keyboard.KeyDownEvent, DirectCast(AddressOf HandleKeyDownEvent, KeyEventHandler))
        End If

        For Each tab As Button In _tabs
            tab.IsEnabled = (tab IsNot _tabToDisable AndAlso Not page = Pages.Loading)
        Next
    End Sub

    Private Sub Tabber_Click(sender As Object, e As RoutedEventArgs) Handles Tabber_Settings.Click, Tabber_Home.Click
        Dim tabber As Button = CType(sender, Button)

        If tabber Is Tabber_Home Then
            SetActivePage(Pages.Main)
        ElseIf tabber Is Tabber_Settings Then
            SetActivePage(Pages.Settings)
        End If
    End Sub
#End Region

#Region "Page: EditSound"
    ''' <summary>
    ''' Sound that is being editted through EditSound page
    ''' </summary>
    Dim EditingSound As Sounds.Sound = Nothing

    ''' <summary>
    ''' Key that is set through EditSound page
    ''' </summary>
    ''' <remarks></remarks>
    Dim _Pressed_Key As Key = Key.None
    Property Pressed_Key As Key
        Get
            Return _Pressed_Key
        End Get
        Set(value As Key)
            Dim setted As Boolean = (Not _Pressed_Key = value)

            _Pressed_Key = value

            If setted Then Trace.WriteLine("Key set to: " + value.ToString)
            If Not setted Then If setted Then Trace.WriteLine("Key is already set to: " + value.ToString)
        End Set
    End Property

    ''' <summary>
    ''' Enabled modifiers through EditSound page
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ReadOnly Property Pressed_Modifs As ModifierKeys
        Get
            Return (New Keys.ModifierKey(Page_EditSound_Shortcut_Alt.IsChecked, Page_EditSound_Shortcut_Ctrl.IsChecked, Page_EditSound_Shortcut_Shift.IsChecked)).ToModifierKeys
        End Get
    End Property

#Region "Modifier keys"
    ''' <summary>
    ''' Is Shift Pressed or Active
    ''' </summary>
    ''' <value></value>
    ''' <returns>IsPressed value of Shift</returns>
    ReadOnly Property Modifier_Shift As Boolean
        Get
            Return (Keyboard.IsKeyDown(Key.LeftShift) Or Keyboard.IsKeyDown(Key.RightShift))
        End Get
    End Property

    ''' <summary>
    ''' Is Control Pressed or Active
    ''' </summary>
    ''' <value></value>
    ''' <returns>IsPressed value of Control</returns>
    ReadOnly Property Modifier_Control As Boolean
        Get
            Return (Keyboard.IsKeyDown(Key.LeftCtrl) Or Keyboard.IsKeyDown(Key.RightCtrl))
        End Get
    End Property

    ''' <summary>
    ''' Is Alt Pressed or Active
    ''' </summary>
    ''' <value></value>
    ''' <returns>IsPressed value of Alt</returns>
    ReadOnly Property Modifier_Alt As Boolean
        Get
            Return (Keyboard.IsKeyDown(Key.LeftAlt) Or Keyboard.IsKeyDown(Key.RightAlt))
        End Get
    End Property

    Function IsModifier(key As Key) As Boolean
        If key = Input.Key.LeftCtrl Or key = Input.Key.RightCtrl Then Return True
        If key = Input.Key.LeftAlt Or key = Input.Key.RightAlt Then Return True
        If key = Input.Key.LeftShift Or key = Input.Key.RightShift Then Return True
        If key = Input.Key.LWin Or key = Input.Key.RWin Then Return True
        If key = Input.Key.System Then Return True

        Return False
    End Function
#End Region

    Private Function getPressedKeys() As List(Of Key)
        Dim ret As New List(Of Key)

        For Each k As Key In [Enum].GetValues(GetType(Key))
            If Not k > 0 Then Continue For
            If Keyboard.IsKeyDown(k) AndAlso IsModifier(k) = False Then ret.Add(k)
        Next

        Return ret
    End Function

    Private Function getPressedModifiers() As ModifierKeys
        Dim ret As ModifierKeys

        If Modifier_Alt Then ret = ret Or ModifierKeys.Alt
        If Modifier_Control Then ret = ret Or ModifierKeys.Control
        If Modifier_Shift Then ret = ret Or ModifierKeys.Shift

        Return ret
    End Function

    'Settings window - Shortcuts/Key-strokes
    Private Sub HandleKeyDownEvent(sender As Object, e As KeyEventArgs)
        If Page_EditSound_Shortcut_Toggler.IsChecked Then
            Trace.WriteLine("**********************************")
            Trace.WriteLine(String.Format("Key: {0}", e.Key))
            Trace.WriteLine(String.Format("IME: {0}", e.ImeProcessedKey))
            Trace.WriteLine(String.Format("System: {0}", e.SystemKey))
            Trace.WriteLine(String.Format("DeadChar: {0}", e.DeadCharProcessedKey))
            Trace.WriteLine("**********************************")

            Dim k As Key = e.Key
            If e.ImeProcessedKey = Key.None AndAlso e.SystemKey = Key.None AndAlso e.DeadCharProcessedKey = Key.None AndAlso IsModifier(k) = False Then
                Pressed_Key = k
                Page_EditSound_Shortcut.Text = Pressed_Key.ToString
            End If
        End If

        'If Page_EditSound_Shortcut_Toggler.IsChecked Then

        '    Trace.WriteLine(String.Format("IME: {0}", e.ImeProcessedKey))
        '    Trace.WriteLine(String.Format("System: {0}", e.SystemKey))
        '    Trace.WriteLine(String.Format("DeadChar: {0}", e.DeadCharProcessedKey))

        '    Pressed_Key = If(IsModifier(e.Key) = False, e.Key, Key.None)
        '    Pressed_Modifs = getPressedModifiers()

        '    'Trace.WriteLine(String.Format("{0} + {1}", Pressed_Modifs, Pressed_Key))
        '    Page_EditSound_Shortcut.Text = String.Format("{0} + {1}", Pressed_Modifs, Pressed_Key)

        '    e.Handled = True
        'End If

        'Trace.WriteLine(String.Format("Not IsModifier({1}): {0}", Not IsModifier(e.Key), e.Key))
        'Trace.WriteLine(String.Format("Any Modifier Pressed: {0}", Keyboard.Modifiers))

        'If Not IsModifier(e.Key) And (Modifier_Alt Or Modifier_Control Or Modifier_Shift) And Not Page_EditSound_Name.IsFocused And Page_EditSound_Shortcut_Toggler.IsChecked Then
        '    Trace.WriteLine("Been there, did that")

        '    Trace.WriteLine(String.Format("{0} | {1}", Keyboard.Modifiers.ToString, e.Key.ToString))
        '    Pressed_Key = e.Key
        '    Pressed_Modifs = Keyboard.Modifiers

        '    Page_EditSound_Shortcut.Text = String.Format("{0} + {1}", Pressed_Modifs, Pressed_Key)

        '    e.Handled = True
        'End If

        'If e.Key = Key.Tab AndAlso (Keyboard.Modifiers And (ModifierKeys.Control Or ModifierKeys.Shift)) = (ModifierKeys.Control Or ModifierKeys.Shift) Then
        '    MessageBox.Show("CTRL + SHIFT + TAB trapped")
        'End If

        'If e.Key = Key.Tab AndAlso (Keyboard.Modifiers And ModifierKeys.Control) = ModifierKeys.Control Then
        '    MessageBox.Show("CTRL + TAB trapped")
        'End If
    End Sub

    Private Sub EditSound_ClearShortcut()
        Pressed_Key = Key.None
        'Pressed_Modifs = ModifierKeys.None

        Page_EditSound_Shortcut_Alt.IsChecked = False
        Page_EditSound_Shortcut_Ctrl.IsChecked = False
        Page_EditSound_Shortcut_Shift.IsChecked = False

        Page_EditSound_Shortcut.Text = "Not Set"
    End Sub

    Private Sub TriggerEdit(snd As Sounds.Sound)
        EditingSound = snd
        EditSound_ClearShortcut()

        Dim nfo As New IO.FileInfo(snd.File)

        Page_EditSound_FileName.Text = IO.Path.GetFileNameWithoutExtension(nfo.FullName)
        Page_EditSound_FileName.ToolTip = snd.File
        Page_EditSound_FileName_Holder.BorderBrush = If(nfo.Exists, Brushes.DodgerBlue, Brushes.Gray)
        Page_EditSound_FileName_Holder.ToolTip = If(nfo.Exists, "File Exists", "File Does Not Exist")

        Page_EditSound_Name.Text = snd.Name

        If Not IsNothing(snd.Hotkey) Then
            Dim modifs As New Keys.ModifierKey(snd.Hotkey.Modifiers)

            Pressed_Key = snd.Hotkey.Key
            'Pressed_Modifs = snd.Hotkey.Modifiers

            Page_EditSound_Shortcut_Alt.IsChecked = modifs.Alt
            Page_EditSound_Shortcut_Ctrl.IsChecked = modifs.Control
            Page_EditSound_Shortcut_Shift.IsChecked = modifs.Shift

            Page_EditSound_Shortcut.Text = If(snd.Hotkey.Key = Key.None, "Not Set", snd.Hotkey.Key.ToString)
        Else
            EditSound_ClearShortcut()
        End If

        SetActivePage(Pages.EditSound)
    End Sub

    Private Sub Page_EditSound_Commands(sender As Object, e As RoutedEventArgs) Handles Page_EditSound_Cancel.Click, Page_EditSound_Submit.Click
        Dim btn As Button = CType(sender, Button)
        Dim canSetPage As Boolean = True

        If btn Is Page_EditSound_Submit Then
            'Do things

            'Assign new name
            EditingSound.AssignName(Page_EditSound_Name.Text)

            'Assign new hotkey - if new
            If EditingSound.GotHotkeyAssigned Then
                Dim hotkey = EditingSound.Hotkey
                Dim sameModifiersAndKey = (Pressed_Key = hotkey.Key AndAlso Pressed_Modifs = hotkey.Modifiers)

                If Not sameModifiersAndKey Then
                    EditingSound.ClearHotkey()
                End If
            End If

            'Unregistered or just not set
            If Not EditingSound.GotHotkeyAssigned Then
                If Not (Pressed_Key = Key.None AndAlso Pressed_Modifs = ModifierKeys.None) AndAlso Not Pressed_Key = Key.None Then
                    'Valid hotkey - only modifiers can be None

                    Dim h As New Hotkeys.Hotkey(Pressed_Key, Pressed_Modifs)
                    hk.RegisterHotkey(h)
                    If h.Registered Then
                        EditingSound.AssignHotkey(h)
                    Else
                        h.Dispose()
                        h = Nothing

                        'TODO: Show error here
                        canSetPage = False
                    End If
                End If
            End If
            'End of things

            If canSetPage Then SetActivePage(Pages.Main)
        ElseIf btn Is Page_EditSound_Cancel Then
            EditingSound = Nothing
            Pressed_Key = Key.None
            'Pressed_Modifs = ModifierKeys.None

            SetActivePage(Pages.Main)
        End If
    End Sub

    Private Sub Page_EditSound_Shortcut_Holder_LostFocus(sender As Object, e As RoutedEventArgs) Handles Page_EditSound_Shortcut_Holder.LostFocus, Page_EditSound_Shortcut_Holder.LostKeyboardFocus
        If Page_EditSound_Shortcut_Toggler.IsChecked Then Page_EditSound_Shortcut_Holder.Focus()
    End Sub

    Private Sub Page_EditSound_Shortcut_Toggler_CheckedChanged(sender As Object, e As RoutedEventArgs) Handles Page_EditSound_Shortcut_Toggler.Checked, Page_EditSound_Shortcut_Toggler.Unchecked
        Dim checked As Boolean = Page_EditSound_Shortcut_Toggler.IsChecked

        If checked Then Page_EditSound_Shortcut_Holder.Focus()
    End Sub

    Private Sub Page_EditSound_Shortcut_Clear_Click(sender As Object, e As RoutedEventArgs) Handles Page_EditSound_Shortcut_Clear.Click
        EditSound_ClearShortcut()
    End Sub
#End Region

#Region "Page: Settings"
    Private Sub VolumeSlider_ValueChanged(sender As Object, e As RoutedPropertyChangedEventArgs(Of Double)) Handles VolumeSlider.ValueChanged
        If Not listenForUIchanges Or IsNothing(cfg) Then Return

        Dim volRounded = Math.Round(VolumeSlider.Value)
        volRounded = VolumeSlider.Value
        Trace.WriteLine("Volume: " + volRounded.ToString + " %")

        cfg.MasterVolume = VolumeSlider.Value
        If (Not IsNothing(sb.PlayedSound)) Then sb.Volume = cfg.MasterVolume_Single
    End Sub

    Private Sub PlaybackDeviceListBox_SelectionChanged(sender As Object, e As SelectionChangedEventArgs) Handles PlaybackDeviceListBox.SelectionChanged
        If Not listenForUIchanges Or IsNothing(cfg) Then Return

        If (PlaybackDeviceListBox.SelectedItem IsNot Nothing) Then cfg.MasterDevice = CType(PlaybackDeviceListBox.SelectedItem.Tag, Integer)
    End Sub
#End Region

    
    Private Sub MainWindow_DragEnter(sender As Object, e As DragEventArgs) Handles Me.DragEnter
        Dropper.Visibility = Windows.Visibility.Visible
    End Sub

    Private Sub MainWindow_DragOver(sender As Object, e As DragEventArgs) Handles Me.DragOver
        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            Dropper.Visibility = Windows.Visibility.Visible
        End If
    End Sub

    Private Sub MainWindow_Drop(sender As Object, e As DragEventArgs) Handles Me.Drop
        Dropper.Visibility = Windows.Visibility.Collapsed

        If e.Data.GetDataPresent(DataFormats.FileDrop) Then
            Dim files = CType(e.Data.GetData(DataFormats.FileDrop), String())

            Trace.WriteLine("************************")
            For Each file In files
                Dim snd As New Sounds.Sound(file)
                sn.RegisterSound(snd)
            Next
            Trace.WriteLine("************************")
        End If
    End Sub

    Private Sub MainWindow_DragLeave(sender As Object, e As DragEventArgs) Handles Me.DragLeave
        Dropper.Visibility = Windows.Visibility.Collapsed
    End Sub
End Class
