﻿Public Class Sounds
    Public Event PlaylistUpdated()
    Public Event TriggeredByHotkey(snd As Sound)

    Private _playlist As New List(Of Sound)
    Public ReadOnly Property Playlist As List(Of Sound)
        Get
            Return _playlist
        End Get
    End Property

    Sub RegisterSound(snd As Sound, Optional InvokeEvent As Boolean = True)
        _playlist.Add(snd)
        AddHandler snd.TriggeredByHotkey, AddressOf TriggeredByHotkey_Sound
        If InvokeEvent Then RaiseEvent PlaylistUpdated()
    End Sub

    Function UnRegisterSound(snd As Sound) As Boolean
        Dim ret = _playlist.Remove(snd)
        RaiseEvent PlaylistUpdated()
        Return ret
    End Function

    Private Sub TriggeredByHotkey_Sound(snd As Sound)
        Trace.WriteLine("The sound was triggered by hotkey", "Kappalina")
        RaiseEvent TriggeredByHotkey(snd)
    End Sub

    Class Sound
        Event TriggeredByHotkey(snd As Sound)
        Event PlayStateChanged()
        Event Updated()

        Private _name As String = ""
        Public ReadOnly Property Name As String
            Get
                Return If(_name = String.Empty, FileNameNoExt, _name)
            End Get
        End Property

        Private _file As String
        Public ReadOnly Property File As String
            Get
                Return _file
            End Get
        End Property

        Public ReadOnly Property FileNameNoExt As String
            Get
                Try
                    Return IO.Path.GetFileNameWithoutExtension(_file)
                Catch ex As Exception
                    Return ""
                End Try
            End Get
        End Property

        Private _hotkey As Hotkeys.Hotkey
        Public ReadOnly Property Hotkey As Hotkeys.Hotkey
            Get
                Return _hotkey
            End Get
        End Property

        Public ReadOnly Property GotHotkeyAssigned As Boolean
            Get
                Return Not IsNothing(_hotkey)
            End Get
        End Property

        Private _playing As Boolean = False
        Public ReadOnly Property PlayState As Boolean
            Get
                Return _playing
            End Get
        End Property

        Sub New(file As String)
            Me._file = file
        End Sub

        Public Sub AssignHotkey(hotkey As Hotkeys.Hotkey)
            Me._hotkey = hotkey

            AddHandler hotkey.Triggered, AddressOf Hotkeyed

            RaiseEvent Updated()
        End Sub

        Public Function ClearHotkey() As Boolean
            If GotHotkeyAssigned Then
                Dim res = _hotkey.UnRegister
                _hotkey = Nothing

                RaiseEvent Updated()

                Return IsNothing(res)
            Else
                Return False
            End If
        End Function

        Public Sub AssignName(name As String)
            _name = name.Trim

            RaiseEvent Updated()
        End Sub

        Public Sub SetPlayState(state As Boolean)
            If state = _playing Then Return
            _playing = state

            If state Then
                Trace.WriteLine("Playing...")
            Else
                Trace.WriteLine("Stopping...")
            End If

            RaiseEvent PlayStateChanged()
        End Sub

        Private Sub Hotkeyed(hotkey As Hotkeys.Hotkey)
            RaiseEvent TriggeredByHotkey(Me)
        End Sub
    End Class

End Class
